let lastUpdateTime = 0;
let toggleTrainingBool = false;
let toggleBinarySpeedBool = false;
let previousSpeed = 0;
let previousRotation = 0.5;

document.getElementById("binarySpeed").addEventListener("mouseover", function() {
    document.getElementById("binarySpeed").style.backgroundColor = "#57a7d6";
});
    
document.getElementById("binarySpeed").addEventListener("mouseout", function() {
    if (toggleBinarySpeedBool){
        document.getElementById("binarySpeed").style.backgroundColor = "#448db6";
    }
    else{
        document.getElementById("binarySpeed").style.backgroundColor = "#2dc3dc";
    }
    });document.getElementById("training").addEventListener("mouseover", function() {
        document.getElementById("training").style.backgroundColor = "#57a7d6";
    });
        
    document.getElementById("training").addEventListener("mouseout", function() {
        if (toggleTrainingBool){
            document.getElementById("training").style.backgroundColor = "#448db6";
        }
        else{
            document.getElementById("training").style.backgroundColor = "#2dc3dc";
        }});


function toggleTraining() {
    if (toggleTrainingBool == false) {
        document.getElementById("training").innerHTML = "DISABLE TRAINING";
        document.getElementById("training").style = "background-color:#448db6";
        toggleTrainingBool = true;

        const formData = new FormData();
        formData.append('training',toggleTrainingBool);           
        const xhr = new XMLHttpRequest();
        xhr.open('POST', '/toggleTraining');
        xhr.send(formData);
    }
    else{
        document.getElementById("training").innerHTML = "ENABLE TRAINING";
        document.getElementById("training").style = "background-color:#2dc3dc";
        toggleTrainingBool = false;

        const formData = new FormData();
        formData.append('training',toggleTrainingBool);           
        const xhr = new XMLHttpRequest();
        xhr.open('POST', '/toggleTraining');
        xhr.send(formData);
    }
}

function toggleBinarySpeed() {
    if (toggleBinarySpeedBool == false) {
        document.getElementById("binarySpeed").innerHTML = "DISABLE BINARY SPEED";
        document.getElementById("binarySpeed").style = "background-color:#448db6";
        toggleBinarySpeedBool = true;
    }
    else{
        document.getElementById("binarySpeed").innerHTML = "ENABLE BINARY SPEED";
        document.getElementById("binarySpeed").style = "background-color:#2dc3dc";
        toggleBinarySpeedBool = false;

    }
}

window.addEventListener('gamepadconnected', (event) => {
    const update = (time) => {

        var speed;
        var rotation;
            
        for (const gamepad of navigator.getGamepads()) {
            if (!gamepad) continue;

            document.getElementById("speed").value = gamepad.buttons[7].value;
            document.getElementById("rotationDot").style = "left:"+(gamepad.axes[0]*42.5+42.5)+"%";
            if (time - lastUpdateTime >= 41) { // Limit à 1/0.044 FPS mais la cam ne bug pas donc cool
                lastUpdateTime = time;

                const formData = new FormData();

                if (toggleBinarySpeedBool==false){
                    speed = Math.round(gamepad.buttons[7].value*document.getElementById("maxSpeed").value)/10;
                }
                else{
                    speed = Math.round(gamepad.buttons[7].value)*document.getElementById("maxSpeed").value/10;
                }

                rotation = (Math.round((gamepad.axes[0]+1)*5))/10;
                
                if (speed!=previousSpeed | rotation!=previousRotation){
                    previousSpeed = speed;
                    previousRotation = rotation
                    formData.append('speed',speed);
                    formData.append('rotation', rotation);
                    const xhr = new XMLHttpRequest();
                    xhr.open('POST', '/sendCommand');
                    xhr.send(formData);
                }
        }

      requestAnimationFrame(update);
    };}
    requestAnimationFrame(update);
  });