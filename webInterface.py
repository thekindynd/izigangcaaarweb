import io
import os
import picamera
import logging
import socketserver
from threading import Condition
from http import server
import cgi
from PIL import Image
import numpy as np
import time

class Car():

    def __init__(self):

        # variables init

        # PWM module parameter
        self.PWM_module = None
        self.PWM_frequency = 60

        # Speed house data
        self.speed_house_data = dict()
        self.speed_house_data["channel"] = 0
        self.speed_house_data["min_signal"] = 0
        self.speed_house_data["max_signal"] = 0
        self.speed_house_data["range_signal"] = self.speed_house_data["max_signal"] - self.speed_house_data["min_signal"]

        # Directionnal servo data
        self.servo_data = dict()
        self.servo_data["channel"] = 1
        self.servo_data["min_signal"] = 0
        self.servo_house_data["max_signal"] = 0
        self.servo_data["range_signal"] = self.speed_house_data["max_signal"] - self.speed_house_data["min_signal"]
        
        # Current State data
        self.curr_direction = 0;
        self.curr_speed = 0;
        self.started = False
        self.learning = False

        # PWM setup
        try:
            from adafruit_servokit import ServoKit
            import board
            import busio
            import adafruit_pca9685
            i2c = busio.I2C(board.SCL, board.SDA)
            self.PWM_module = adafruit_pca9685.PCA9685(i2c)
            self.PWM_module = self.PWM_frequency

        except Exception as e:
            print("Erreur lors de la préparation du pilote de servo")
            print('Erreur retournée : ', e)


    def set_speed(self,r):
        # Input : a float in [0,1]
        # Result : Create a PWM signal on the speed house channel 
        #that present a proportionnal duty-cycle
        self.curr_direction = r
        dc = r * self.speed_house_data["range_signal"] + self.speed_house_data["min_signal"]
        self.PWM_module.channels[self.speed_house_data["channel"]].duty_cycle = dc

    def set_dir(self,r):
        # Input : a float in [0,1]
        # Result : Create a PWM signal on the servo channel 
        #that present a proportionnal duty-cycle
        self.curr_direction = r
        dc = r * self.servo_data["range_signal"] + self.servo_data["min_signal"]
        self.PWM_module.channels[self.servo_data["channel"]].duty_cycle = dc

class StreamingOutput(object):
    def __init__(self):
        self.frame = None
        self.buffer = io.BytesIO()
        self.condition = Condition()

    def write(self, buf):
        if buf.startswith(b'\xff\xd8'):
            # New frame, copy the existing buffer's content and notify all
            # clients it's available
            self.buffer.truncate()
            with self.condition:
                self.frame = self.buffer.getvalue()
                self.condition.notify_all()
            self.buffer.seek(0)
        return self.buffer.write(buf)

class StreamingHandler(server.BaseHTTPRequestHandler):

    speed=0
    rotation=10
    training = False
    nImage = 0
    
    def do_GET(self):
        if self.path == '/':
            self.send_response(301)
            self.send_header('Location', '/index.html')
            self.end_headers()
        elif self.path == '/index.html':
            self.send_response(200)
            self.send_header('Content-Type', 'text/html')
            self.end_headers()
            self.speed=0
            self.rotation=0
            with open('index.html', 'rb') as file: 
                    self.wfile.write(file.read()) # Read the file and send the contents 
        
        elif self.path.startswith('/styles/'):
            self.send_response(200)
            self.send_header('Content-Type', 'text/css')
            self.end_headers()
            with open("."+self.path, 'rb') as file: 
                    self.wfile.write(file.read()) # Read the file and send the contents 

        elif self.path.startswith('/javascript/'):
            self.send_response(200)
            self.send_header('Content-Type', 'text/js')
            self.end_headers()
            with open("."+self.path, 'rb') as file: 
                    self.wfile.write(file.read()) # Read the file and send the contents 
        
        elif self.path.startswith('/ressources/'):
            self.send_response(200)
            self.send_header('Content-Type', 'text/img')
            self.end_headers()
            with open("."+self.path, 'rb') as file: 
                    self.wfile.write(file.read()) # Read the file and send the contents 

        elif self.path == '/stream.mjpg':
            self.send_response(200)
            self.send_header('Age', 0)
            self.send_header('Cache-Control', 'no-cache, private')
            self.send_header('Pragma', 'no-cache')
            self.send_header('Content-Type', 'multipart/x-mixed-replace; boundary=FRAME')
            self.end_headers()
            try:
                while True:
                    with output.condition:
                        output.condition.wait()
                        frame = output.frame
                    self.wfile.write(b'--FRAME\r\n')
                    self.send_header('Content-Type', 'image/jpeg')
                    self.send_header('Content-Length', len(frame))
                    self.end_headers()
                    self.wfile.write(frame)
                    self.wfile.write(b'\r\n')
                    if (StreamingHandler.training==True):
                        image_name = 'training/frame_'+str(StreamingHandler.nImage)+'_gas_'+str(StreamingHandler.speed)+'_dir_'+str(StreamingHandler.rotation)+'.jpg'
                        if not os.path.exists(os.path.dirname(image_name)):
                            os.makedirs(os.path.dirname(image_name))
                        with open(image_name, 'wb') as f:
                            f.write(frame)
                        StreamingHandler.nImage += 1

            except Exception as e:
                logging.warning(
                    'Removed streaming client %s: %s',
                    self.client_address, str(e))

        else:
            self.send_error(404)
            self.end_headers()
    
    def do_POST(self):

        if (self.path == "/sendCommand"):
            form = cgi.FieldStorage(
                fp=self.rfile,
                headers=self.headers,
                environ={'REQUEST_METHOD': 'POST', 'CONTENT_TYPE': self.headers['Content-Type']}
            )
            StreamingHandler.speed = form.getvalue('speed')
            #Call to the speed function
            print("speed:")
            car.set_speed(StreamingHandler.speed)

            StreamingHandler.rotation = form.getvalue('rotation')
            #call to the rotation function
            print("direction:")
            car.set_direction(StreamingHandler.rotation)

            self.send_response(201)
            self.end_headers()

        elif (self.path == "/toggleTraining"):
            form = cgi.FieldStorage(
                fp=self.rfile,
                headers=self.headers,
                environ={'REQUEST_METHOD': 'POST', 'CONTENT_TYPE': self.headers['Content-Type']}
            )

            if form.getvalue('training')=="false":
                StreamingHandler.training=False
            else:
                StreamingHandler.training=True

            self.send_response(201)
            self.end_headers()         

        else:
            self.send_response(400)
            self.end_headers()
        
class StreamingServer(socketserver.ThreadingMixIn, server.HTTPServer):
    allow_reuse_address = True
    daemon_threads = True

with picamera.PiCamera(resolution='640x480', framerate=20) as camera:
    output = StreamingOutput()
    #Uncomment the next line to change the Pi's Camera rotation (in degrees)
    #camera.rotation = 90
    camera.start_recording(output, format='mjpeg')
    try:
        address = ('', 8000)
        server = StreamingServer(address, StreamingHandler)
        server.serve_forever()
    finally:
        camera.stop_recording()
