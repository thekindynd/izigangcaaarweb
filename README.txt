Il faut exécuter le code sur un raspberry possédant une Picamera activée via "sudo raspi-config"

Lancer webInterface.py avec "python3 webInterface.py"

Si des problèmes d'importation sont trouvés alors installer les packages à import avec pip "pip3 install {package_name}"

Pour vous connecter à l'interface web depuis un ordinateur connecté au même réseau que la raspberry:
    1. Exécuter "hostname -I" sur la raspberry, mémoriser la première ip renvoyée
    2. Entrer dans son navigateur favoris http://{adresse_ip}:8000" et appuyer sur entré.

La barre de progression sous la vidéo de la caméra indique l'intensité de votre pression sur la gachette, la barre de direction indique la position de votre joystick sur l'axe horizontal.

Pour contrôler la rotation et la vitesse branchez une manette de console. Le joystick devrait contrôler la rotation et la gachette arrière droite la vitesse.

Les valeurs de vitesse et de direction sont discrétisées en 10 valeurs différentes.

Changez la valeur de "Maximum speed" à votre guise pour définir la vitesse maximale, entre 1 et 10, que votre gachette vous permettra d'atteindre.

Activer le "ENABLE BINARY SPEED" pour que la valeur de vitesse envoyée ne varie qu'entre 0 et la valeur maximum de vitesse.

Activer le "ENABLE TRAINING" enregistrera chaque frame capturée avec la commande exécutée lors de sa capture.